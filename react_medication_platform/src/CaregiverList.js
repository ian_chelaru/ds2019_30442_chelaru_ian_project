import React, { Component } from 'react';
import { Button, ButtonGroup, Container, Table } from 'reactstrap'
import { Link } from 'react-router-dom';

class CaregiverList extends Component {

    emptyCaregiver = {
        id: ''
    };

    constructor(props) {
        super(props);
        this.state = { caregivers: [], isLoading: true, caregiverToDelete: this.emptyCaregiver };
        this.remove = this.remove.bind(this);
    }

    componentDidMount() {
        this.setState({ isLoading: true });

        fetch('/api/caregivers')
            .then(response => response.json())
            .then(data => this.setState({ caregivers: data, isLoading: false }));
    }

    async remove(id) {
        let caregiver = { ...this.state.emptyCaregiver };
        caregiver['id'] = id;
        this.setState({ caregiverToDelete: caregiver });
        await fetch('/api/caregivers', {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(caregiver),
        }).then(() => {
            let updatedCaregivers = [...this.state.caregivers].filter(i => i.id !== id);
            this.setState({ caregivers: updatedCaregivers });
        });
    }

    render() {
        const { caregivers, isLoading } = this.state;

        if (isLoading) {
            return <p>Loading...</p>;
        }

        const caregiverList = caregivers.map(caregiver => {
            return <tr key={caregiver.id}>
                <td>{caregiver.name}</td>
                <td>{caregiver.birthDate}</td>
                <td>{caregiver.gender}</td>
                <td>{caregiver.address}</td>
                <td>
                    <ButtonGroup>
                        <Button size="sm" color="primary" tag={Link} to={"/caregivers/" + caregiver.id}>Edit</Button>
                        <Button size="sm" color="danger" onClick={() => this.remove(caregiver.id)}>Delete</Button>
                    </ButtonGroup>
                </td>
            </tr>
        });

        return (
            <div>
                <Container fluid>
                    <div className="float-right">
                        <Button color="success" tag={Link} to={"caregivers/new/"}>Add Caregiver</Button>
                    </div>
                    <h3>Caregivers</h3>
                    <Table className="mt-4">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Birth Date</th>
                                <th>Gender</th>
                                <th>Address</th>
                            </tr>
                        </thead>
                        <tbody>
                            {caregiverList}
                        </tbody>
                    </Table>
                </Container>
            </div>
        );
    }
}

export default CaregiverList;