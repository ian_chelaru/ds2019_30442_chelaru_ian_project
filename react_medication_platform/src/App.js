import React, { Component } from 'react'
import './App.css';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import CaregiverList from './CaregiverList';
import CaregiverEdit from './CaregiverEdit';
import PatientList from './PatientList';
import PatientEdit from './PatientEdit';
import MedicationList from './MedicationList';
import MedicationEdit from './MedicationEdit';
import DoctorHome from './DoctorHome.js';
import CaregiverHome from './CaregiverHome.js';
import PatientHome from './PatientHome.js';
import Login from './Login.js';
import MedicationPlanEdit from './MedicationPlanEdit.js';
import AddPrescription from './AddPrescription';
import ActivityInfo from "./ActivityInfo";

class App extends Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route path="/" component={Login} exact={true} />
          <Route path='/caregivers' exact={true} component={CaregiverList} />
          <Route path='/caregivers/:id' component={CaregiverEdit} />
          <Route path='/patients' exact={true} component={PatientList} />
          <Route path='/patients/:id' component={PatientEdit} />
          <Route path='/medications' exact={true} component={MedicationList} />
          <Route path='/medications/:id' component={MedicationEdit} />
          <Route path='/doctor' component={DoctorHome} />
          <Route path='/caregiver' component={CaregiverHome} />
          <Route path='/patient' component={PatientHome} />
          <Route path='/prescriptions/patient/:id' component={MedicationPlanEdit} />
          <Route path='/addpresciption/patient/:id' component={AddPrescription} />
          <Route path="/activities/patient/:id" component={ActivityInfo}/>
        </Switch>
      </Router>
    )
  }
}

export default App;
