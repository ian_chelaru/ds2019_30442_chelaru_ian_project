import React, { Component } from 'react';
import { Container, Button, Table } from 'reactstrap';
import { Link } from 'react-router-dom';

class MedicationPlanEdit extends Component {
    constructor(props) {
        super(props);
        this.state = { prescriptions: [] };
    }

    componentDidMount() {
        let patientId = this.props.match.params.id;

        fetch(`/api/prescriptions/patient/${patientId}`)
            .then(response => response.json())
            .then(data => this.setState({ prescriptions: data }));
    }

    render() {
        const { prescriptions } = this.state;
        let patientId = this.props.match.params.id;

        const prescriptionList = prescriptions.map(prescription => {
            return <tr key={prescription.id}>
                <td>{prescription.medication.name}</td>
                <td>{prescription.dailyInterval}</td>
                <td>{prescription.startDate}</td>
                <td>{prescription.endDate}</td>
            </tr>
        });

        return (
            <div>
                <Container fluid>
                    <div className="float-right">
                        <Button color="success" tag={Link} to={"/addpresciption/patient/" + patientId}>Add Prescription</Button>
                    </div>
                    <h3>Medical Plan</h3>
                    <Table className="mt-4">
                        <thead>
                            <tr>
                                <th>Medication Name</th>
                                <th>Daily Interval</th>
                                <th>Start Date</th>
                                <th>End Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            {prescriptionList}
                        </tbody>
                    </Table>
                </Container>
            </div >
        );
    }
}

export default MedicationPlanEdit;