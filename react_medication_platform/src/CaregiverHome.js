import React, { Component } from 'react';
import { Container, Table } from 'reactstrap';
import Stomp from 'stompjs';
import SockJS from 'sockjs-client';

class CaregiverHome extends Component {
    emptyActivity = {
        patientId: '',
        name: '',
        start: '',
        end: ''
    };

    constructor(props) {
        super(props);
        const user = this.props.location.state.user;
        this.state = { user: user, patients: [] };
    }

    notification() {
        var Stomp = require('stompjs');
        var socket = new SockJS('/websocket');
        var stompClient = Stomp.over(socket);
        console.log("BEFORE CONNECT");
        stompClient.connect({},
            function (frame) {
                console.log("Connection successful");
                console.log('Connected: ' + frame);
                stompClient.subscribe('/topic/notify', function (activity) {
                    console.log(activity);
                    alert(activity);
                    // var activityObj = activity.body.json();
                    // console.log(activityObj.name);
                    // console.log(JSON.parse(greeting.body).content);
                });
            }, () => {
                console.log("Connection error");
            });
        console.log("AFTER CONNECT");
    }

    componentDidMount() {
        const user = this.state.user;
        fetch(`/api/patients/caregiver/${user.personId}`)
            .then(response => response.json())
            .then(data => this.setState({ patients: data }));
        this.notification();
    }

    render() {
        const { user, patients } = this.state;

        const patientList = patients.map(patient => {
            return <tr key={patient.id}>
                <td>{patient.name}</td>
                <td>{patient.birthDate}</td>
                <td>{patient.gender}</td>
                <td>{patient.address}</td>
            </tr>
        });

        return <div>
            <h1>Hello {user.username}</h1>
            <div>
                <Container fluid>
                    <h2>Your Pacients</h2>
                    <Table className="mt-4">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Birth Date</th>
                                <th>Gender</th>
                                <th>Address</th>
                            </tr>
                        </thead>
                        <tbody>
                            {patientList}
                        </tbody>
                    </Table>
                </Container>
            </div>
        </div>
    }
}

export default CaregiverHome;