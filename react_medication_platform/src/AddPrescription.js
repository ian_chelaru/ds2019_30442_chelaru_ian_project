import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { Container, Form, FormGroup, Label, Input, Button } from 'reactstrap';

class AddPrescription extends Component {

    emptyPrescription = {
        medicationName: 'empty',
        dailyInterval: '',
        startDate: new Date(),
        endDate: new Date(),
        patientId: ''
    };

    constructor(props) {
        super(props);
        this.state = { prescription: this.emptyPrescription, medications: [] }
        this.submitHandler = this.submitHandler.bind(this);
    }

    componentDidMount() {
        fetch('/api/medications')
            .then(response => response.json())
            .then(data => this.setState({ medications: data }));
    }

    async submitHandler(event) {
        event.preventDefault();
        let { prescription } = this.state;
        let id = this.props.match.params.id;

        prescription.patientId = id;
        console.log(prescription);
        fetch('/api/prescriptions', {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(prescription)
        });
        let url = '/prescriptions/patient/' + id;
        this.props.history.push({ pathname: url, props: {id: id}, state: { prescriptions: [] } });
    }

    changeHandler = (event) => {
        let nam = event.target.name;
        let val = event.target.value;
        let newPrescription = { ...this.state.prescription };
        newPrescription[nam] = val;
        this.setState({ prescription: newPrescription });
    }

    render() {
        let { prescription, medications } = this.state;
        let id = this.props.match.params.id;

        const medicationList = medications.map(medication => {
            return <option key={medication.name} value={medication.name}>
                {medication.name}
            </option>
        });

        return <div>
            <Container>
                <h2>Add Prescription</h2>
                <Form onSubmit={this.submitHandler}>
                    <FormGroup>
                        <Label for="medicationName">Medication</Label>
                        <select id="medication_name" name="medicationName" onChange={this.changeHandler} value={this.medicationName}>
                            <option value="empty"></option>
                            {medicationList}
                        </select>
                    </FormGroup>
                    <FormGroup>
                        <Label for="daily_interval">Daily Interval</Label>
                        <Input type="text" id="daily_interval" name="dailyInterval" value={prescription.dailyInterval || ''} onChange={this.changeHandler} />
                    </FormGroup>
                    <FormGroup>
                        <Label for="start_date">Start Date</Label>
                        <Input type="date" id="start_date" name="startDate" value={prescription.startDate || ''} onChange={this.changeHandler}></Input>
                    </FormGroup>
                    <FormGroup>
                        <Label for="end_date">End Date</Label>
                        <Input type="date" id="end_date" name="endDate" value={prescription.endDate || ''} onChange={this.changeHandler}></Input>
                    </FormGroup>
                    <FormGroup>
                        <Button type="submit" color="primary">Save</Button>{' '}
                        <Button color="secondary" tag={Link} to={"/prescriptions/patient/" + id}>Cancel</Button>
                    </FormGroup>
                </Form>
            </Container>
        </div>
    }
}

export default withRouter(AddPrescription);