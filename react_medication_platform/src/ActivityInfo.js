import React, { Component } from 'react';
import CanvasJSReact from './canvasjs.react';

var CanvasJSChart = CanvasJSReact.CanvasJSChart;

class ActivityInfo extends Component {
    constructor(props) {
        super(props);
        this.state = { activities: [] };
    }

    componentDidMount() {
        let patientId = this.props.match.params.id;

        fetch(`/api/activities/patient/${patientId}`)
            .then(response => response.json())
            .then(data => this.setState({ activities: data }));
    }

    printActivities() {
        const { activities } = this.state;
        console.log(activities);
    }

    render() {
        this.printActivities();
        console.log("qqqqqqqqqqqqqqqqqqqqqqqqqqq");

        const options = {
            exportEnabled: true,
            animationEnabled: true,
            title: { text: "Activity History" },
            data: [{
                type: "pie",
                indexLabel: "{label}: {y} hours",
                startAngle: -90,
                dataPoints: [
                    { y: 20, label: "Airfare" },
                    { y: 24, label: "Food & Drinks" },
                    { y: 20, label: "Accomodation" },
                    { y: 14, label: "Transportation" },
                    { y: 12, label: "Activities" },
                    { y: 10, label: "Misc" }
                ]
            }]
        }

        return (
            <div>
                <CanvasJSChart options={options}
                />
            </div>
        );
    }
}

export default ActivityInfo;