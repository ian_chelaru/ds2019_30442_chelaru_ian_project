Open a command prompt in /src/main/java and run these commands:

1. set path=%path%;C:\Program Files\Java\jdk1.8.0_211\bin
2. wsimport -keep -p jaxws.activity http://localhost:8081/activityservice?wsdl
3. wsimport -keep -p jaxws.recommendation http://localhost:8081/recommendationservice?wsdl