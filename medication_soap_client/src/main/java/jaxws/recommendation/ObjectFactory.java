
package jaxws.recommendation;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the jaxws.recommendation package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _InsertRecommendationResponse_QNAME = new QName("http://services/", "insertRecommendationResponse");
    private final static QName _InsertRecommendation_QNAME = new QName("http://services/", "insertRecommendation");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: jaxws.recommendation
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link InsertRecommendationResponse }
     * 
     */
    public InsertRecommendationResponse createInsertRecommendationResponse() {
        return new InsertRecommendationResponse();
    }

    /**
     * Create an instance of {@link InsertRecommendation }
     * 
     */
    public InsertRecommendation createInsertRecommendation() {
        return new InsertRecommendation();
    }

    /**
     * Create an instance of {@link Recommendation }
     * 
     */
    public Recommendation createRecommendation() {
        return new Recommendation();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InsertRecommendationResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services/", name = "insertRecommendationResponse")
    public JAXBElement<InsertRecommendationResponse> createInsertRecommendationResponse(InsertRecommendationResponse value) {
        return new JAXBElement<InsertRecommendationResponse>(_InsertRecommendationResponse_QNAME, InsertRecommendationResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InsertRecommendation }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services/", name = "insertRecommendation")
    public JAXBElement<InsertRecommendation> createInsertRecommendation(InsertRecommendation value) {
        return new JAXBElement<InsertRecommendation>(_InsertRecommendation_QNAME, InsertRecommendation.class, null, value);
    }

}
