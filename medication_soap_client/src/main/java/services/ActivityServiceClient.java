package services;

import jaxws.activity.Activity;
import jaxws.activity.ActivityService;
import jaxws.activity.ActivityServiceImplService;

import java.net.URL;
import java.util.List;

public class ActivityServiceClient
{
    private static final String spec = "http://localhost:8081/activityservice?wsdl";

    private ActivityService activityService;

    public ActivityServiceClient() throws Exception
    {
        URL url = new URL(spec);
        ActivityServiceImplService activityServiceImplService = new ActivityServiceImplService(url);
        this.activityService = activityServiceImplService.getActivityServiceImplPort();
    }

    public List<Activity> getActivities()
    {
        return activityService.getActivitiesByPatientId(1);
    }
}
