package services;

import jaxws.recommendation.Recommendation;
import jaxws.recommendation.RecommendationService;
import jaxws.recommendation.RecommendationServiceImplService;

import java.net.URL;

public class RecommendationServiceClient
{
    private static final String spec = "http://localhost:8081/recommendationservice?wsdl";

    private RecommendationService recommendationService;

    public RecommendationServiceClient() throws Exception
    {
        URL url = new URL(spec);
        RecommendationServiceImplService recommendationServiceImplService = new RecommendationServiceImplService(url);
        recommendationService = recommendationServiceImplService.getRecommendationServiceImplPort();
    }

    public void addRecommendation(Recommendation recommendation)
    {
        recommendationService.insertRecommendation(recommendation);
    }
}
