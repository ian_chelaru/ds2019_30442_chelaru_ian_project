package com.ianchelaru.medication_platform;

import com.ianchelaru.medication_platform.consumer.Receive;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

@SpringBootApplication(exclude = {SecurityAutoConfiguration.class})
public class MedicationPlatformApplication {

	public static void main(String[] args) {
		SpringApplication.run(MedicationPlatformApplication.class, args);
		try
		{
			Receive.receive();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

}
