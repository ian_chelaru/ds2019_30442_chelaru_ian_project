package com.ianchelaru.medication_platform.service;

import com.ianchelaru.medication_platform.enitities.Role;
import com.ianchelaru.medication_platform.enitities.User;
import com.ianchelaru.medication_platform.errorhandler.ResourceNotFoundException;
import com.ianchelaru.medication_platform.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService
{
    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository)
    {
        this.userRepository = userRepository;
    }

    public Integer insert(User user)
    {
        user.setRole(Role.PATIENT);
        return userRepository.save(user).getId();
    }

    public User findByUsername(String username)
    {
        return userRepository.findByUsername(username).orElseThrow(
                () -> new ResourceNotFoundException("User","username",username));
    }
}
