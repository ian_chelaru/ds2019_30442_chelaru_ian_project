package com.ianchelaru.medication_platform.service;

import com.ianchelaru.medication_platform.dto.PrescriptionDTO;
import com.ianchelaru.medication_platform.enitities.Medication;
import com.ianchelaru.medication_platform.enitities.Patient;
import com.ianchelaru.medication_platform.enitities.Prescription;
import com.ianchelaru.medication_platform.repository.MedicationRepository;
import com.ianchelaru.medication_platform.repository.PatientRepository;
import com.ianchelaru.medication_platform.repository.PrescriptionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PrescriptionService {

    private final PrescriptionRepository prescriptionRepository;
    private final PatientService patientService;
    private final MedicationService medicationService;

    @Autowired
    public PrescriptionService(PrescriptionRepository prescriptionRepository, PatientService patientService, MedicationService medicationService) {
        this.prescriptionRepository = prescriptionRepository;
        this.patientService = patientService;
        this.medicationService = medicationService;
    }

    public List<Prescription> findAllByPatientId(Integer id) {
        return prescriptionRepository.findByPatient_id(id);
    }

    public Integer insert(PrescriptionDTO prescriptionDTO) {
        Medication medication = medicationService.findByName(prescriptionDTO.getMedicationName());
        Patient patient = patientService.findById(prescriptionDTO.getPatientId());
        Prescription prescription = new Prescription(medication, prescriptionDTO.getDailyInterval(), prescriptionDTO.getStartDate(),
                prescriptionDTO.getEndDate(), patient);
        return prescriptionRepository.save(prescription).getId();
    }
}
