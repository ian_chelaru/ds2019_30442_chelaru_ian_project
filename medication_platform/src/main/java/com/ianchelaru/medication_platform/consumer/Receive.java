package com.ianchelaru.medication_platform.consumer;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;
import org.json.*;
import org.json.simple.JSONObject;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.messaging.simp.SimpMessagingTemplate;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;

public class Receive
{
    private final static String QUEUE_NAME = "activities";
    private final static long HOUR = 3600000;

    public static void receive() throws  Exception
    {
        ConnectionFactory factory = new ConnectionFactory();
        //        factory.setHost("localhost");
        factory.setHost("rabbitmqcontainer");

        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.queueDeclare(QUEUE_NAME,false,false,false,null);
        System.out.println(" [*] Waiting for messages");

        DeliverCallback deliverCallback = ((consumerTag,delivery) ->
        {
            String message = new String(delivery.getBody(),StandardCharsets.UTF_8);
            System.out.println(" [x] Received '" + message + "'");
            ActivityPostRequest.postActivity(message);
            if (isAnomalous(message))
            {
                ActivityPostRequest.notifyCaregiver(message);
                System.err.println("Anomalous activity: " + message);
            }
        });

        channel.basicConsume(QUEUE_NAME,true,deliverCallback,consumerTag ->
        {
        });
    }

//    public static void main(String[] args) throws Exception
//    {
//        ConnectionFactory factory = new ConnectionFactory();
////        factory.setHost("localhost");
//        factory.setHost("rabbitmqcontainer");
//        Connection connection = factory.newConnection();
//        Channel channel = connection.createChannel();
//
//        channel.queueDeclare(QUEUE_NAME,false,false,false,null);
//        System.out.println(" [*] Waiting for messages");
//
//        DeliverCallback deliverCallback = ((consumerTag,delivery) ->
//        {
//            String message = new String(delivery.getBody(),StandardCharsets.UTF_8);
//            System.out.println(" [x] Received '" + message + "'");
//            ActivityPostRequest.postActivity(message);
//            if (isAnomalous(message))
//            {
//                ActivityPostRequest.notifyCaregiver(message);
//                System.err.println("Anomalous activity: " + message);
//            }
//        });
//
//        channel.basicConsume(QUEUE_NAME,true,deliverCallback,consumerTag ->
//        {
//        });
//    }

    private static boolean isAnomalous(String jsonActivity)
    {
        JsonReader jsonReader = Json.createReader(new StringReader(jsonActivity));
        JsonObject jsonObject = jsonReader.readObject();
        String activity = jsonObject.getString("activity");
        long start = jsonObject.getJsonNumber("start").longValue();
        long end = jsonObject.getJsonNumber("end").longValue();
        long activityDuration = end - start;
        if ("Sleeping".equals(activity) && (activityDuration > 12 * HOUR))
        {
            return true;
        }
        if ("Leaving".equals(activity) && (activityDuration > 12 * HOUR))
        {
            return true;
        }
        return "Toileting".equals(activity) && (activityDuration > HOUR);
    }

}
