
package com.ianchelaru.medication_platform.jaxws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.ianchelaru.medication_platform.jaxws package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetActivitiesByPatientIdResponse_QNAME = new QName("http://services/", "getActivitiesByPatientIdResponse");
    private final static QName _GetActivitiesByPatientId_QNAME = new QName("http://services/", "getActivitiesByPatientId");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.ianchelaru.medication_platform.jaxws
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetActivitiesByPatientIdResponse }
     * 
     */
    public GetActivitiesByPatientIdResponse createGetActivitiesByPatientIdResponse() {
        return new GetActivitiesByPatientIdResponse();
    }

    /**
     * Create an instance of {@link GetActivitiesByPatientId }
     * 
     */
    public GetActivitiesByPatientId createGetActivitiesByPatientId() {
        return new GetActivitiesByPatientId();
    }

    /**
     * Create an instance of {@link Activity }
     * 
     */
    public Activity createActivity() {
        return new Activity();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetActivitiesByPatientIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services/", name = "getActivitiesByPatientIdResponse")
    public JAXBElement<GetActivitiesByPatientIdResponse> createGetActivitiesByPatientIdResponse(GetActivitiesByPatientIdResponse value) {
        return new JAXBElement<GetActivitiesByPatientIdResponse>(_GetActivitiesByPatientIdResponse_QNAME, GetActivitiesByPatientIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetActivitiesByPatientId }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services/", name = "getActivitiesByPatientId")
    public JAXBElement<GetActivitiesByPatientId> createGetActivitiesByPatientId(GetActivitiesByPatientId value) {
        return new JAXBElement<GetActivitiesByPatientId>(_GetActivitiesByPatientId_QNAME, GetActivitiesByPatientId.class, null, value);
    }

}
