package com.ianchelaru.medication_platform.controller;

import com.ianchelaru.medication_platform.enitities.Patient;
import com.ianchelaru.medication_platform.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/patients")
public class PatientController
{
    private final PatientService patientService;

    @Autowired
    public PatientController(PatientService patientService)
    {
        this.patientService = patientService;
    }

    @GetMapping(value = "/{id}")
    public Patient findById(@PathVariable("id") Integer id)
    {
        return patientService.findById(id);
    }

    @GetMapping
    public List<Patient> findAll()
    {
        return patientService.findAll();
    }

    @PostMapping
    public Integer insert(@RequestBody Patient patient)
    {
        return patientService.insert(patient);
    }

    @PutMapping
    public Integer update(@RequestBody Patient patient)
    {
        return patientService.update(patient);
    }

    @DeleteMapping
    public void delete(@RequestBody Patient patient)
    {
        patientService.delete(patient);
    }

    @GetMapping(value = "/caregiver/{id}")
    public List<Patient> findAllByCaregiverId(@PathVariable("id") Integer id)
    {
        return patientService.findAllByCaregiverId(id);
    }

}
