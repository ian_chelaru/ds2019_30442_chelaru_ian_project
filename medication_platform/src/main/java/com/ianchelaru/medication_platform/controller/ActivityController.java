package com.ianchelaru.medication_platform.controller;

import com.ianchelaru.medication_platform.dto.ActivityDTO;
import com.ianchelaru.medication_platform.jaxws.Activity;
import com.ianchelaru.medication_platform.jaxws.ActivityServiceImplService;
import com.ianchelaru.medication_platform.service.ActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URL;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/activities")
public class ActivityController
{
    private final ActivityService activityService;

    @Autowired
    public ActivityController(ActivityService activityService)
    {
        this.activityService = activityService;
    }

    @PostMapping
    public Integer insert(@RequestBody ActivityDTO activityDTO)
    {
        return activityService.insert(activityDTO);
    }

    @GetMapping(value = "/patient/{id}")
    public List<Activity> findAllByPatientId(@PathVariable("id") Integer id) throws Exception
    {
        URL url = new URL("http://localhost:8081/activityservice?wsdl");

        ActivityServiceImplService activityWebServiceImpl = new ActivityServiceImplService(url);

        com.ianchelaru.medication_platform.jaxws.ActivityService activityWebService =
                activityWebServiceImpl.getActivityServiceImplPort();
        return activityWebService.getActivitiesByPatientId(id);
    }
}
