package com.ianchelaru.medication_platform.controller;

import com.ianchelaru.medication_platform.dto.PrescriptionDTO;
import com.ianchelaru.medication_platform.enitities.Prescription;
import com.ianchelaru.medication_platform.service.PrescriptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/prescriptions")
public class PrescriptionController {

    private final PrescriptionService prescriptionService;

    @Autowired
    public PrescriptionController(PrescriptionService prescriptionService) {
        this.prescriptionService = prescriptionService;
    }

    @PostMapping
    public Integer insert(@RequestBody PrescriptionDTO prescriptionDTO)
    {
        return prescriptionService.insert(prescriptionDTO);
    }

    @GetMapping(value = "/patient/{id}")
    public List<Prescription> findAllByPatientId(@PathVariable("id") Integer id)
    {
        return prescriptionService.findAllByPatientId(id);
    }
}
