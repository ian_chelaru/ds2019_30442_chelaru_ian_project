//package com.ianchelaru.medication_platform.enitities;
//
//import lombok.Getter;
//import lombok.Setter;
//
//import javax.persistence.*;
//import java.util.Set;
//
//import static javax.persistence.GenerationType.IDENTITY;
//
//@Entity
//@Getter
//@Setter
//public class MedicationPlan
//{
//    @Id
//    @GeneratedValue(strategy = IDENTITY)
//    @Column(unique = true, nullable = false)
//    private Integer id;
//
//    @OneToMany(mappedBy = "medicationPlan")
//    private Set<Prescription> prescriptions;
//
//    @OneToOne(mappedBy = "medicationPlan")
//    private Patient patient;
//}
