package com.ianchelaru.medication_platform.enitities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Getter
@Setter
public class Patient
{
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(unique = true, nullable = false)
    private Integer id;

    @Column(nullable = false)
    private String name;

    @Column
    private Date birthDate;

    @Column(nullable = false)
    private String gender;

    @Column
    private String address;

    @ManyToOne
    @JoinColumn(name = "caregiver_id")
    @JsonIgnore
    private Caregiver caregiver;

    @OneToMany(mappedBy = "patient")
    private Set<Prescription> prescriptions;

    @OneToMany(mappedBy = "patient")
    private Set<Activity> activities;

//    @OneToOne(cascade = CascadeType.ALL)
//    @JoinColumn(name = "medication_plan_id", referencedColumnName = "id")
//    @JsonIgnore
//    private MedicationPlan medicationPlan;
}
