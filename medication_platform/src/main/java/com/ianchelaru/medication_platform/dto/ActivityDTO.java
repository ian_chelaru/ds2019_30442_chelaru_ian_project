package com.ianchelaru.medication_platform.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ActivityDTO {
    @JsonProperty("patient_id")
    private Integer patientId;

    @JsonProperty("activity")
    private String name;

    private Long start;

    private Long end;
}
