package com.ianchelaru.medication_platform.repository;

import com.ianchelaru.medication_platform.enitities.Prescription;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PrescriptionRepository extends JpaRepository<Prescription, Integer>
{
    List<Prescription> findByPatient_id(Integer id);
}
