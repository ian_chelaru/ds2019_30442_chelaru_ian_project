package com.ianchelaru.medication_platform.repository;

import com.ianchelaru.medication_platform.enitities.Caregiver;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CaregiverRepository extends JpaRepository<Caregiver, Integer>
{
    List<Caregiver> findByOrderByNameAsc();
}
