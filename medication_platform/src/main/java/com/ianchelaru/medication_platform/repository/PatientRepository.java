package com.ianchelaru.medication_platform.repository;

import com.ianchelaru.medication_platform.enitities.Caregiver;
import com.ianchelaru.medication_platform.enitities.Patient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PatientRepository extends JpaRepository<Patient, Integer>
{
    List<Patient> findByCaregiver_id(Integer id);
}
