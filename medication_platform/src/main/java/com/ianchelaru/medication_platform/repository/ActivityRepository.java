package com.ianchelaru.medication_platform.repository;

import com.ianchelaru.medication_platform.enitities.Activity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ActivityRepository extends JpaRepository<Activity, Integer> {
}
