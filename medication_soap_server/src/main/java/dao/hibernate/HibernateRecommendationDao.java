package dao.hibernate;

import dao.hibernate.util.HibernateUtil;
import entities.Recommendation;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class HibernateRecommendationDao
{
    private SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

    public void insert(Recommendation recommendation) throws RuntimeException
    {
        try
        {
            Session currentSession = sessionFactory.openSession();
            Transaction transaction = currentSession.beginTransaction();
            currentSession.merge(recommendation);
            transaction.commit();
            currentSession.close();
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    }
}
