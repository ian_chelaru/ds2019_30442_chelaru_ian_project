import services.ActivityServiceImpl;
import services.RecommendationServiceImpl;

import javax.xml.ws.Endpoint;

public class ServicePublisher
{
    public static void main(String[] args)
    {
//        Endpoint.publish("http://localhost:8081/activityservice",new ActivityServiceImpl());
        Endpoint.publish("http://0.0.0.0:8081/activityservice",new ActivityServiceImpl());
//        Endpoint.publish("http://localhost:8081/recommendationservice",new RecommendationServiceImpl());
        Endpoint.publish("http://0.0.0.0:8081/recommendationservice",new RecommendationServiceImpl());
    }
}
