package entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Getter
@Setter
@Table(name = "activity")
public class Activity
{
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(unique = true, nullable = false)
    private Integer id;

    @Column(name = "patient_id", nullable = false)
    private Integer patientId;

    @Column(name = "activity", nullable = false)
    private String name;

    @Column(name = "start", nullable = false)
    private Long start;

    @Column(name = "end", nullable = false)
    private Long end;

    @Override
    public String toString()
    {
        return "Activity{" + "id=" + id + ", patientId=" + patientId + ", name='" + name + '\'' + ", start=" + start +
                ", end=" + end + '}';
    }
}
