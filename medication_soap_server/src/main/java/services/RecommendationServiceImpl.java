package services;

import dao.hibernate.HibernateRecommendationDao;
import entities.Recommendation;

import javax.jws.WebService;

@WebService(endpointInterface = "services.RecommendationService")
public class RecommendationServiceImpl implements RecommendationService
{
    private HibernateRecommendationDao hibernateRecommendationDao = new HibernateRecommendationDao();

    public void insertRecommendation(Recommendation recommendation)
    {
        hibernateRecommendationDao.insert(recommendation);
    }
}
