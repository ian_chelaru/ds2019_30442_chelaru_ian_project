package services;

import entities.Recommendation;

import javax.jws.WebService;

@WebService
public interface RecommendationService
{
    void insertRecommendation(Recommendation recommendation);
}
