package services;

import dao.hibernate.HibernateActivityDao;
import entities.Activity;

import javax.jws.WebService;
import java.util.List;

@WebService(endpointInterface = "services.ActivityService")
public class ActivityServiceImpl implements ActivityService
{
    private HibernateActivityDao hibernateActivityDao = new HibernateActivityDao();

    public List<Activity> getActivitiesByPatientId(int patientId)
    {
        return hibernateActivityDao.findAllByPatientId(patientId);
    }
}
